Our team of family and pediatric dentists are dedicated to providing high quality and affordable dental care for your entire family. We emphasize compassionate and gentle care to help both children and adult patients feel more at ease while we take care of your dental needs.

Address: 204, Ignacio Comonfort 9350, Zona Urbana Rio Tijuana, B.C 22320, Mexico

Phone: +52 664 684 7285

Website: https://www.dentiland.net/
